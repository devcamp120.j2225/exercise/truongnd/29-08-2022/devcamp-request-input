 // import  thư viện express.js vào. Dạng Import express from "express";
 const express = require('express');

 // khởi tạo app express
 const app = express();

 //khai báo sẵn 1 port trên hệ thống
 const port = 8000;

 //khai bao để app đọc được request body json
 app.use(express.json());

 // Khai báo API
app.get("/", (rep,res) => {
    let today = new Date();

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        message : `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1 } năm ${today.getFullYear()}`
    })
});

// Request param
// Thường được sử dụng với GET,PUT,DELETE
// Được đính sẵn vào url
app.get("/request-params/:param1/:param2/:param", (req,res) =>{
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        param : {
            param1,
            param2
        }
    })
});

// Request query
// Thường chỉ được sử dụng cho GET
// Do không kiểm soát được request query, nên cần bắt buộc validate request query
app.get("/request-query", (req,res) =>{
    let body = req.query;

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        query
    })
});

//Request body json
// Thường dùng POST,PUT
// Do không kiểm soát được request body json, nên cần bắt buộc validate request body json
// cần khai báo dòng 11 để vào json
app.post("/request-body-json", (req,res) =>{
    let body = req.body;

    // Response trả 1 chuỗi json có status 200
    res.status(200).json({
        body
    })
});

 // Khơi tạo app
 // Callback function
 app.listen(port, () =>{
    console.log("App listening on port: ",port);
 });